import { Component, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http'

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {

  constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {
  }

  Logout() {
    console.log("Logout");

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    //return this.http.post(this.url, book, options)
    this.http.post(this.baseUrl + '/Account/Logout', "", options).subscribe(result => {
      //console.log("Logged out");
      location.replace("/")
    }, error => console.error(error));
  }

}
